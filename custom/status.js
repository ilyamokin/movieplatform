function Status(total, persent_step) {
	this.total = total;
	this.current = 0;
	this.current_persent = 0;
	this.persent_step = persent_step;
}

Status.prototype.show = function (count) {
	this.current += (count || 1);
	var newPercent = (this.current / this.total * 100).toFixed(0);

	if (newPercent - this.current_persent >= this.persent_step) {
		this.current_persent = newPercent;
		console.log('status: %s% | %s records | %s', this.current_persent, this.current, (new Date).toLocaleTimeString());
	}
};

module.exports = Status;