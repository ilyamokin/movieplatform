require("should");

var SRV = require("../../services/youtube/Youtube");

describe('youtubeSrv tests', function () {
	this.timeout(5000);

	it('order and obtained data', function (done) {
		var videoOps = {},
			categoryOps = {},
			model = {
				video        : null,
				allCategories: null
			},
			order = 0;

		SRV
			.getProvider()
			.searchMovies({q: "auto", maxResults: 10})
			.exec(function (err, data) {
				videoOps.id = data.items[0].id.videoId;
				data.items.length.should.be.above(0);
				//console.log(JSON.stringify(data));
				(order++).should.be.eql(0);
			})
			.getVideo(videoOps)
			.exec(function (err, data) {
				model.video = data.items[0];
				console.log(JSON.stringify(data));
				data.items.length.should.be.eql(1);
				(order++).should.be.eql(1);
			})
			.getCategories(categoryOps)
			.exec(function (err, data) {
				model.allCategories = data.items;
				data.items.length.should.be.above(0);
				(order++).should.be.eql(2);
				done();
			});
	});
});