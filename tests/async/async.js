require("should");

var Async = require(".././async"),
	async = new Async();

describe('async tests', function () {
	this.timeout(4000);
	it('order', function (done) {
		var times = 0;
		setTimeout(async(function (param) {
			(times++).should.be.eql(0);
			param.should.be.eql("0");
		}), 500, "0");

		async(function (param) {
			(times++).should.be.eql(1);
			param.should.be.eql("1");
		}).ready("1");

		setTimeout(async(function () {
			(times++).should.be.eql(2);
			done();
		}), 100);
	});

	it('with arguments', function (done) {
		setTimeout(async(function (param1, param2) {
			param1.should.be.eql("param1");
			param2.should.be.eql("param2");
			done();
		}), 0, "param1", "param2");
	});
});