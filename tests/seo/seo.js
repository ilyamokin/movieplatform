require("should");

var SRV = require("../../services/seo");

describe('seo tests', function () {
	it('description words', function () {
		SRV.getFilteredWords('word1 word2 and http:/.com word3, abc,word4, word5: word6')
			.should.eql([ 'word1', 'word2', 'word3', 'word4', 'word5', 'word6' ]);
	});
});