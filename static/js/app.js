angular.module('platform', ['ngMaterial', 'ngMdIcons', 'ngMessages'])
	.config(function ($mdThemingProvider) {
		var customBlueMap = 		$mdThemingProvider.extendPalette('light-blue', {
			'contrastDefaultColor': 'light',
			'contrastDarkColors': ['50'],
			'50': 'ffffff'
		});
		$mdThemingProvider.definePalette('customBlue', customBlueMap);
		// Update the theme colors to use themes on font-icons
		$mdThemingProvider
			.theme('default').primaryPalette('customBlue', {
			'default': '500',
			'hue-1'  : '50'
		})
	});