module.exports = function (grunt) {
	var config = {
		less            : {
			src: {
				files: [
					{
						expand: true,
						cwd   : 'public/default/less',
						src   : '**/*.less',
						dest  : 'public/default/css',
						ext   : '.css'
					}
				]
			}
		}
	};

	grunt.initConfig(config);
	grunt.loadNpmTasks('grunt-contrib-less');
};
