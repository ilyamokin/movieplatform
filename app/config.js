module.exports = {
	partials : [
		'/views/shared-partials'
	],
	salt     : '$2a$10$WVAOzlDcCNuXAW92hw5hbe',
	mongoUrl : 'mongodb://localhost:27017/movieplatform',
	filesPath: 'static/uploaded',
	server   : {
		port: 1337
	},
	youtube  : {
		token: "AIzaSyBpFuygegvyvD1oSRFodaQDSj09NmJG17k"
	},
	linkedin : {
		appId : '77zoorhtkgmp3m',
		secret: 'eJOXO55grPydsERd'
	},
	twitter  : {
		appId : '9z3K90U5tafHdJMnKhauX3YQ9',
		secret: 'bIh0jVj6d8sMRQ2MKyZSpxhnEPSpePPsiH9otqU1mnamQIF4JR'
	},
	vkontakte: {
		appId : '5085925',
		secret: 'xgjGzEVdf2tWSljfO9nI'
	},
	facebook : {
		appId      : '988962044460536',
		secret     : '2e589b1a211f09d5442a7634e1742e2e',
		enableProof: true
	}
};
