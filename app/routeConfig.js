module.exports = {
	init:function(router){
		var movieCtrl = require('../controllers/movie'),
			fileCtrl  = require('../controllers/file');

		router.route('/').all(movieCtrl.list);

		router.route('/image/:type/:id').all(fileCtrl.image);

		router.route('/image/:type/:id/:seoKey').all(fileCtrl.image);

		router.route('/movieframe/:id').all(movieCtrl.frame);

		router.route('/:controller/:action').all(function (req, resp) {
			try {
				require('../controllers/{0}'.format(req.params.controller))
						[req.params.action].apply(this, arguments);
			} catch (e) {
				resp.status(404);
				resp.render('404', {title: 'Not Found :('});
			}
		});

		router.route('/:controller/:action/:id').all(function (req, resp) {
			require('../controllers/{0}'.format(req.params.controller))
					[req.params.action].apply(this, arguments);
		});

		router.route('/:controller/:action/:id/:seoKey').all(function (req, resp) {
			require('../controllers/{0}'.format(req.params.controller))
					[req.params.action].apply(this, arguments);
		});
		return router;
	}
};
