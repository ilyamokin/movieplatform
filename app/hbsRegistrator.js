module.exports = {
	registerPartials: function () {
		var hbs    = require('express-hbs'),
			fs               = require('fs'),
			config = require('./config');

		config.partials.forEach(function (dir) {
			var partialsDir = '{0}/..{1}'.format(__dirname, dir);
			fs.readdirSync(partialsDir)
				.forEach(function (filename) {
					var matches = /^([^.]+).hbs$/.exec(filename);
					if (!matches) {
						return;
					}
					var name = matches[1],
						templateText = fs.readFileSync('{0}/{1}'.format(partialsDir, filename), 'utf8');
					hbs.registerPartial(name, templateText);
				});
		});
	},
	registerHelpers: function () {
		var hbs    = require('express-hbs');

		hbs.registerHelper('ifvalue', function (conditional, options) {
			if (options.hash.value === conditional) {
				return options.fn(this);
			} else {
				return options.inverse(this);
			}
		});
	}
};
