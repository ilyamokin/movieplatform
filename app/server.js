require('./../custom/string-extensions');

var express          = require('express'),
	router           = express.Router(),
	path             = require('path'),
	hbsRegistrator   = require('./hbsRegistrator'),
	config           = require('./config'),
	defaultRouter    = require('./routeConfig'),
	session          = require('express-session'),
	passport         = require('passport'),
	MongoStore       = require('connect-mongo')(session),
	passportSettings = require('../services/loginStrategies/passportSettings'),
	app              = express(),
	monk             = require('monk'),
	db               = monk(config.mongoUrl);

hbsRegistrator.registerHelpers();
hbsRegistrator.registerPartials();

app
	.set('port', process.env.PORT || config.server.port || 1337)
	.engine('hbs', require('express-hbs').express4())
	.set('views', path.join(__dirname, '/../views'))
	.set('view engine', 'hbs');

require('../services/loginStrategies/facebook').init(router);
require('../services/loginStrategies/vkontakte').init(router);
require('../services/loginStrategies/linkedin').init(router);
require('../services/loginStrategies/twitter').init(router);
require('../services/loginStrategies/local').init(router, db);

app
	.use(require('compression')())
	.use(require('morgan')('dev'))
	.use(require('body-parser')())
	.use(express.static(path.join(__dirname, '/../static')))
	.use(session({
		cookie           : {
			maxAge: 3600000 * 24 * 20//1h * 24 * 20
		},
		saveUninitialized: true,
		resave           : false,
		store            : new MongoStore({
			url: config.mongoUrl
		}),
		secret           : config.salt,
		rolling          : true,
		autoReconnect    : true
	}));

passportSettings.setup(passport, app);

app.use(function (req, res, next) {
	req.db = db;
	next();
});

app
	.use(defaultRouter.init(router))
	.use(function (req, res) {
		res
			.status(404)
			.render('shared/err404', {title: 'Not Found :('});
	});

if (app.get('env') === 'development') {
	app.use(require('errorhandler')());
}


app.listen(app.get('port'), function () {
	console.log('Express app listening on port ' + app.get('port'));
});

