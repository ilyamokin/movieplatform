'use strict'
let co = require('co');

module.exports = {
	login     : co.wrap(function* (req, res, next) {
		if (!req.body.login && !req.user) {
			res.render('account/login', {user: req.user});
		} else if (req.body.login) {
			let passport = require('passport'),
				bcrypt   = require('bcrypt'),
				config   = require('../app/config'),
				login    = req.body.login.trim(),
				pass     = bcrypt.hashSync(req.body.password, config.salt),
				user     = yield req.db.get('users').findOne({username: login, password: pass});

			passport.authenticate('local', function (err) {
				if (err) { return next(err); }
				if (!user) { return res.render('account/login', {user: req.user}); }
				req.logIn({
					displayName: login,
					id         : user._id,
					provider   : 'local'
				}, function (err) {
					if (err) { return next(err); }
					return res.redirect('/');
				});
			})(req, res, next)
		} else {
			res.redirect('/');
		}
	}),
	logout    : function (req, res) {
		req.logOut();
		res.redirect('/');
	},
	register  : function (req, res) {
		if (!req.user) {
			res.render('account/register', {user: {loginError: ""}});
		} else {
			res.redirect('/');
		}
	},
	createUser: function (req, res, next) {
		var newUser = req.body;

		newUser.errors = [];
		new Promise(function (resolve, reject) {
			req.db.get('users').count({username: newUser.login, provider: 'local'}, function (err, count) {
				if (count) {
					newUser.errors.push("Login already exists");
				}
				if (newUser.password != newUser.confirm) {
					newUser.errors.push("Please check the password and confirmation");
				}
				resolve(newUser);
			});
		}).then(function (newUser) {
			if (newUser.errors && newUser.errors.length) {
				res.render('account/register', {user: newUser});
			} else {
				var bcrypt   = require('bcrypt'),
					config   = require('../app/config'),
					ObjectID = require('mongodb').ObjectID;
				req.db.get('users').insert({
					_id     : new ObjectID(),
					username: newUser.login,
					password: bcrypt.hashSync(newUser.password, config.salt),
					provider: 'local'
				}, function (err, user) {
					var passport = require('passport');

					passport.authenticate('local', function (err) {
						if (err) { return next(err); }
						if (!user) { return res.redirect('/login'); }
						req.logIn({
							displayName: user.username,
							id         : user._id,
							provider   : user.provider
						}, function (err) {
							if (err) { return next(err); }
							return res.redirect('/');
						});
					})(req, res, next)
				});
			}
		}, function (err) {
			res.send({error: err.message || err});
		});
	}

};
