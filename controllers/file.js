module.exports = {
	image: function (req, res) {
		var Srv,
			getUrl,
			image = require('dyson-image'),
			type  = req.params.type,
			id    = req.params.id;

		if (type == 'y') {
			Srv = require("../services/youtube/Youtube").YouTube;
			getUrl = function (data) {
				return {
					remote: data.items[0].snippet.thumbnails.medium.url,
					local : data.localImagePath
				}
			};
		} else {
			Srv = require("../services/rutube/RutubeSrv").Rutube;
			getUrl = function (data) {
				return {
					remote: '{0}?size=s'.format(data.thumbnail_url),
					local : data.localImagePath
				};
			};
		}

		new Srv(req.db)
			.getVideoImage(id)
			.then(function (resp) {
				var data           = resp.video,
					provider       = resp.provider,
					url            = getUrl(data),
					requestToImage = function () {
						image.request({
							host: url.remote
						}).then(function (imageObj) {
							var imgBase64    = imageObj.buffer.toString('base64'),
								buf          = new Buffer(imgBase64, 'base64'),
								fileName     = id + url.remote.match(/\/([\w\d\.]+)(\?|$)/)[1],
								FleUploader  = require('../services/fileUploader').FileUploader,
								fileUploader = new FleUploader();


							fileUploader.uploadImage(fileName, buf)
								.then(function (imagePath) {
									provider.updateImagePath(id, imagePath);
									res.writeHead(200, {
										'Content-Type': 'image/jpeg',
										'Content-Length': buf.length
									});
									res.end(buf);
								}, function (err) {
									res.send({error: err.message || err});
								});


						});
					};

				if (url.local) {
					res.sendFile(url.local, {root: '{0}/../'.format(__dirname)}, function (err) {
						if (err) {
							console.log(err);
							requestToImage();
						}
					});
				} else {
					requestToImage();
				}
			});
	}
};
