'use strict';

var co = require('co');

module.exports = {
	list: function (req, res) {
		var rpage = req.query.rp || 1,
			ypage = req.query.rp == 1 ? null : req.query.yp;

		getListMovies(req.db, req.query.q || '', rpage, ypage, 12, true)
			.then(function (model) {
				model.user = req.user;
				model.isAuthenticated = req.isAuthenticated();
				res.render('movie/list', model);
			});
	},

	setrate: function (req, res) {
		let rate     = +req.query.rate || 1,
			ObjectID = require('mongodb').ObjectID,
			id       = new ObjectID(req.query.id);
		req.db.get('movies').update({_id: id}, {
			$inc: {
				rate : rate,
				votes: 1
			}
		});
		res.redirect(req.header('Referer'));
	},

	rid: function (req, res) {
		var Rutube     = require("../services/rutube/RutubeSrv").Rutube,
			formatter  = require("../services/formatter"),
			seo        = require("../services/seo"),
			movieModel = {},
			_          = require('lodash');

		new Rutube(req.db)
			.getVideo(req.params.id)
			.then(function (args) {
				var video = args.video,
					tags  = seo.getRandomKeywords(seo.getFilteredWords(video.description), video.title).slice(0, 25),
					q     = seo.getFilteredWords(video.title).slice(0, 3).join(' ');
				movieModel = {
					keywords     : tags.join(', '),
					title        : video.title,
					movie        : {
						localId       : args._id,
						id            : req.params.id,
						rate          : args.rate || 1,
						provider      : 'r',
						title         : video.title,
						hasPlayerHtml : true,
						playerHtml    : video.html,
						description   : video.description,
						hasDescription: video.description.length > 0,
						hasTags       : tags.length > 0,
						tags          : tags,
						comments      : args.comments
					},
					relatedMovies: []
				};
				return getListMovies(q, 1, null, 5);

			}).then(function (resp) {
			if (movieModel.error) return;

			movieModel.relatedMovies = _.map(resp.movies, function (item) {
				return {
					id      : item.id,
					title   : item.title,
					type    : item.type,
					seoTitle: seo.getSeoKey(item.title)
				}
			});
			movieModel.user = req.user;
			movieModel.isAuthenticated = req.isAuthenticated();
			res.render('movie/movie', movieModel);
		}).catch(function (err) {
			if (err) {
				console.error(err.message);
			}
		});
	},

	yid: function getMovieById(req, res) {
		var YouTube       = require("../services/youtube/Youtube").YouTube,
			formatter     = require("../services/formatter"),
			seo           = require("../services/seo"),
			movieModel    = {},
			_             = require('lodash'),
			searchMoviesQ = {q: '', maxResults: 6};

		new YouTube(req.db)
			.getVideo(req.params.id)
			.then(function (resp) {
				var data = resp.video;

				if (!data.items.length) {
					movieModel.error = true;
					res.render('movie/movieHasBeenRemoved', {});
					return;
				}
				var movie = data.items[0],
					desc  = movie.snippet.description;

				movie.snippet.description = formatter.descriptionFormat(desc);
				movie.containsDescription = desc.length > 0;
				movie.tags = seo.getRandomKeywords(seo.getFilteredWords(desc), movie.snippet.title).slice(0, 25);
				searchMoviesQ.q = seo.getFilteredWords(movie.snippet.title).slice(0, 3).join(' ');
				movieModel = {
					keywords: movie.tags.join(', '),
					title        : movie.snippet.title,
					movie   : {
						localId       : resp._id,
						id            : req.params.id,
						rate          : resp.rate || 1,
						provider      : 'y',
						title         : movie.snippet.title,
						hasPlayerHtml : false,
						description   : movie.snippet.description,
						hasDescription: movie.snippet.description.length > 0,
						hasTags       : movie.tags.length > 0,
						tags          : movie.tags,
						comments      : resp.comments
					}
				};
				return getListMovies(searchMoviesQ.q, 1, null, 5);
			})
			.then(function (resp) {
				if (movieModel.error) return;

				movieModel.relatedMovies = _.map(resp.movies, function (item) {
					return {
						id      : item.id,
						title   : item.title,
						type    : item.type,
						seoTitle: seo.getSeoKey(item.title)
					}
				});

				movieModel.user = req.user;
				movieModel.isAuthenticated = req.isAuthenticated();
				res.render('movie/movie', movieModel);
			});

	},

	frame: function getFrameById(req, res) {
		res.render('movie/movieframe', {
			movieId: req.params.id
		});
	},

	addComment: co.wrap(function* (req, res) {
		var ObjectID = require('mongodb').ObjectID,
			id       = req.query.i,
			provider = req.query.p == 'y' ? 'youtube' : 'rutube';

		let result = yield req.db.get('movies').update({
				providerId: id,
				provider  : provider
			},
			{
				$addToSet: {
					comments: {
						id      : new ObjectID(),
						comment : req.body.comment,
						userId  : req.user.id,
						userName: req.user.displayName
					}
				}
			},
			{multi: true});
		res.redirect(req.headers.referer);
	})
};

function getListMovies(db, query, rutubePage, youtubePage, count, withCategories) {
	var model   = {
			movies    : [],
			categories: []
		},
		Rutube  = require("./../services/rutube/RutubeSrv").Rutube,
		YouTube = require("./../services/youtube/Youtube").YouTube,
		seo     = require("./../services/seo"),
		_       = require('lodash'),
		pair    = seo.getRandomDigitPair(count, query + rutubePage + youtubePage);

	return Promise.all([
		new YouTube(db)
			.searchMovies({q: query || '', pageToken: youtubePage || null, maxResults: pair.firstNum})
			.then(function (resp) {
				var data = resp.movies;

				if (data) {
					model.prevYoutubePage = data.prevPageToken;
					model.nextYoutubePage = data.nextPageToken;
					model.movies.push.apply(model.movies, data.items.map(function (m) {
						return {
							id    : m.id.videoId,
							title : m.snippet.title,
							type  : 'y',
							seoKey: seo.getSeoKey(m.snippet.title)
						}
					}));
				}

				return model;
			}),
		withCategories ? new YouTube().getCategories({})
			.then(function (resp) {
				var data = resp.categories;
				model.categories.push.apply(model.categories, _.map(data.items, function (cat) {
					return {
						name: cat.snippet.title
					}
				}));
				return model;
			}) : model,

		new Rutube(db)
			.searchMovies({q: query || '', page: rutubePage || null, limit: pair.secondNum})
			.then(function (resp) {
				var data = resp.movies || [];

				if (data) {
					model.prevRutubePage = data.page - 1;
					model.nextRutubePage = data.page + 1;
					if (model.prevRutubePage < 1) {
						model.hasNotBackBtn = true;
					}
					model.movies.push.apply(model.movies, data.results.map(function (m) {
						return {
							id    : m.id,
							title : m.title,
							type  : 'r',
							seoKey: seo.getSeoKey(m.title)
						}
					}));
				}
				return model;
			}),
		withCategories ? new Rutube().getCategories().then(function (resp) {
			var data = resp.categories;
			model.categories.push.apply(model.categories, _.map(data.results, function (cat) {
				return {
					name: cat.name
				}
			}));
			return model;
		}) : model
	]).then(function () {
		var firstMovie = _.first(model.movies);
		model.title = query ? 'Search Result: {0}'.format(query) : 'Latest Uploads';
		model.movies = seo.shuffle(model.movies, firstMovie ? firstMovie.title : model.title);
		model.categories = seo.shuffle(model.categories, firstMovie ? firstMovie.title : model.title, 40);
		return model;
	});
}
