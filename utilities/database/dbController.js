require('./../../custom/string-extensions');

var config = require('../configs/config'),
	mongoClient = require('mongodb').MongoClient;

mongoClient.connect(config.connectionString, function (err, db) {
	if (err) throw err;

	var collection = db.collection(config.dbCollection);
	collection.remove({'fullData.snippet': {$exists:true}, $where: 'this.fullData.snippet.description.length < 500 '})

});
