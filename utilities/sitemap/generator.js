'use strict';
require('./../../custom/string-extensions');

let config      = require('./config'),
	mongoClient = require('mongodb').MongoClient,
	fs          = require('fs'),
	sitemap     = require('sitemap-xml'),
	co          = require('co'),
	Status      = require('../../custom/status');


co(function*() {
	let db = yield mongoClient.connect(config.connectionString);

	let updateDate = new Date().format("yyyy-mm-dd"),
		xmlStream  = sitemap(),
		fileStream = fs.createWriteStream(
			config.location.format(updateDate + '_r'),
			{flags: 'w', encoding: null, mode: 666});

	xmlStream.pipe(fileStream);

	let movies = yield db.collection('sitemap').find({provider: 'rutube', description: /.{100}/gmi, done: {$exists: 0}}).toArray();

	let status = new Status(movies.length, 5);
	for (let movie of movies) {
		xmlStream.write({
			loc     : `http://naparom.ru/movie/rid/${movie.providerId.split('_')[0]}`,
			lastmod : updateDate,
			priority: '0.9'
		});
		status.show();
	}
	xmlStream.end();

	yield db.collection('sitemap').updateMany({provider: 'rutube', description: /.{100}/gmi, done: {$exists: 0}}, {$set: {done: true}});
	db.close();
}).catch(err=>console.error(err));


co(function*() {
	let db = yield mongoClient.connect(config.connectionString);

	let updateDate = new Date().format("yyyy-mm-dd"),
		xmlStream  = sitemap(),
		fileStream = fs.createWriteStream(
			config.location.format(updateDate + '_y'),
			{flags: 'w', encoding: null, mode: 666});

	xmlStream.pipe(fileStream);

	let movies = yield db.collection('sitemap').find({provider: 'youtube', description: /.{150}/gmi, done: {$exists: 0}}).toArray();

	let status = new Status(movies.length, 5);
	for (let movie of movies) {
		xmlStream.write({
			loc     : `http://naparom.ru/movie/yid/${movie.providerId.split('_')[0]}`,
			lastmod : updateDate,
			priority: '0.9'
		});
		status.show();
	}
	xmlStream.end();

	yield db.collection('sitemap').updateMany({provider: 'youtube', description: /.{150}/gmi, done: {$exists: 0}}, {$set: {done: true}});
	db.close();
}).catch(err=>console.error(err));