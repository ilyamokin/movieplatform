require('./../../custom/string-extensions');

var mongoClient = require('mongodb').MongoClient,
	config = require('./config');

mongoClient.connect(config.connectionStringFrom, function (err, dbFrom) {
	mongoClient.connect(config.connectionStringTo, function (err, dbTo) {
		var to = dbTo.collection('youtube');

		dbFrom.collection('youtube').find().each(function (err, doc) {
			if (!doc) {
				return;
			}
			to.insert(doc, function () {});
		})
	});
});
