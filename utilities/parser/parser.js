"use strict";

let MongoClient   = require('mongodb').MongoClient,
	ObjectID      = require('mongodb').ObjectID,
	config        = require('./../configs/config'),
	co            = require('co'),
	fs            = require('fs'),
	itemsForQuery = 50,
	db;

co(function*() {
	db = yield MongoClient.connect(config.connectionString);
	let Youtube     = require("../../services/youtube/Youtube").YouTube,
		Rutube      = require("../../services/rutube/RutubeSrv").Rutube,
		youtubePage = null,
		text        = fs.readFileSync('../configs/queries.txt', 'utf8'),
		lines       = text.split('\r\n'),
		parsed      = 0,
		addedTotal  = 0,
		addedY      = 0,
		addedR      = 0,
		queries     = [],
		movies      = [],
		progress    = 0;

	yield db.collection('sitemap').ensureIndex({providerId: 1}, {unique: true, dropDups: true, background: true});

	for (let query of lines) {
		let _addedTotal = addedTotal,
			_addedY     = addedY,
			_addedR     = addedR,
			_parsed     = parsed;

		for (let i = 10, page = 1; i <= itemsForQuery; i += 10, page += 1) {
			let youtube = new Youtube(db),
				rutube  = new Rutube(db);
			let data = yield {
				youtube: youtube.searchMovies({q: query, pageToken: youtubePage, maxResults: 10}),
				rutube : rutube.searchMovies({q: query, page: page, limit: 10})
			};

			let rutubeMovies = (data.rutube.movies.results || []).map(r=> {
				return {
					provider   : 'rutube',
					providerId : `${r.id}_rutube`,
					description: r.description,
					title      : r.title,
					response   : r
				}
			});

			for (let m of rutubeMovies) {
				let result = yield db.collection('sitemap').update({providerId: m.providerId}, m, {upsert: true});
				addedTotal += result.result.nModified ? 0 : 1;
				addedR += result.result.nModified ? 0 : 1;
				if (result.result.upserted && result.result.upserted.length) {
					movies.push(result.result.upserted[0]._id);
				}
			}

			let youtubeMovies = (data.youtube.movies.items || []).map(r=> {
				return {
					provider   : 'youtube',
					providerId : `${r.id.videoId}_youtube`,
					description: r.snippet.description,
					title      : r.snippet.title,
					response   : r
				}
			});

			for (let m of youtubeMovies) {
				let result = yield db.collection('sitemap').update({providerId: m.providerId}, m, {upsert: true});
				addedTotal += result.result.nModified ? 0 : 1;
				addedY += result.result.nModified ? 0 : 1;
				if (result.result.upserted && result.result.upserted.length) {
					movies.push(result.result.upserted[0]._id);
				}
			}

			youtubePage = data.youtube.movies.nextPageToken;

			parsed += youtubeMovies.length + rutubeMovies.length;
		}

		queries.push({
			query     : query,
			addedTotal: addedTotal - _addedTotal,
			addedY    : addedY - _addedY,
			addedR    : addedR - _addedR,
			parsed    : parsed - _parsed
		});
		progress = (lines.indexOf(query)+1) / lines.length * 100;

		console.log(`${progress.toFixed(0)}%, added: ${addedTotal}, Y: ${addedY}, R ${addedR},  parsed ${parsed}, ${query}`)
	}

	let statistic = {
		date      : new Date(),
		addedTotal: addedTotal,
		addedY    : addedY,
		addedR    : addedR,
		parsed    : parsed,
		queries   : queries,
		movies    : movies
	};


	yield db.collection('sitemap_sessions').insertOne(statistic);
	delete statistic.movies;
	console.log(statistic);
	db.close();
}).then(()=>db.close()).catch(err=>console.error(err));
