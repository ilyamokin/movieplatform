'use strict';
module.exports.FileUploader = function () {

	this.uploadImage = function (fileName, buf) {
		var fs         = require('fs'),
			mkdirp     = require("mkdirp"),
			getDirName = require("path").dirname,
			config     = require('../app/config'),
			imagePath  = "{0}/images/{1}".format(config.filesPath, fileName),
			Q          = require('q');

		return Q.Promise(function (resolve, reject) {
			mkdirp(getDirName(imagePath), function (err) {
				if (err) {
					reject(err);
				}
				fs.writeFile(imagePath, buf, function (err) {
					if (err) {
						reject(err);
					}
					resolve(imagePath);
				});
			});
		});
	}

};