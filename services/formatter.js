'use strict';
var ctx = {
	wrapLists         : function (str) {
		str = str.replace(/(<p> *\n*)*^[·-](.|\n)*[·-].+/gmi, '<ul>$&</ul>');
		str = str.replace(/^(<ul>)*(<p> *\n*)*[·-](.+)<\/p>/gmi, '$1<li> $3 </li>');
		return str;
	},
	splitByRow        : function (str) {
		return str.replace(/^([\w \d\W]+?)$/gmi, '<p>$&</p>');
	},
	wrapUpperCaseWords: function (str) {
		return str.replace(/\s[A-Z]{4,}[-A-Z\d:-?&$]\s*/gm, '<b>$&</b>')
	},
	descriptionFormat : function (description) {
		description = ctx.splitByRow(description);
		description = ctx.wrapUpperCaseWords(description);
		description = ctx.wrapLists(description);

		return description;
	}
};

module.exports = ctx;