'use strict';

module.exports.Rutube = function (db) {
	var request  = require('request'),
		provider = this,
		ObjectID = require('mongodb').ObjectID,
		co       = require('co'),
		ctx      = this;

	this.searchMovies = co.wrap(function* (ops) {
		ops.page = ops.page || 1;
		ops.limit = ops.limit || 10;

		return yield new Promise(function (resolve, reject) {
			var query = ops.q || "popular";
			request(`http://rutube.ru/api/search/video/?query=${query}&page=${ops.page}&limit=${ops.limit}&format=json`, function (error, response, body) {
				try {
					if(body) {
						resolve({provider: provider, movies: JSON.parse(body)});
					}else{
						resolve({provider: provider, movies: []});
					}
				} catch (err) {
					console.log(body);
					throw err;
				}
			});
		});
	});

	this.getVideoImage = co.wrap(function* (id) {
		return yield ctx.getVideo(id, ['json.thumbnail_url', 'json.localImagePath'])
	});

	this.updateImagePath = function (id, imagePath) {
		var movies = db.get('movies');
		return new Promise(function (resolve, reject) {
			movies.updateOne(
				{provider: 'rutube', providerId: id},
				{$set: {'json.localImagePath': imagePath}}, function (err, resp) {
					resolve({provider: provider});
				});
		});
	};

	this.getVideo = co.wrap(function* (id, fields) {
		var movies = db.get('movies'),
			video  = yield movies.findOne({provider: 'rutube', providerId: id}, fields || {});

		if (video) {
			return yield Promise.resolve({provider: provider, rate: (video.rate / video.votes).toFixed(0), video: video.json, _id: video._id, comments: video.comments});
		} else {
			return yield new Promise(function (resolve) {
				request(`http://rutube.ru/api/video/${id}?format=json`, function (error, response, body) {
					video = JSON.parse(body);
					let new_id = new ObjectID(),
						rate   = +((Math.random() * 10) % 3 + 2).toFixed(0);
					movies.insert({
						_id       : new_id,
						provider  : 'rutube',
						rate      : rate * 50,
						votes     : 50,
						providerId: id,
						json      : video,
						comments  : []
					});

					resolve({provider: provider, video: video, _id: id, comments: []});
				});
			})
		}
	});

	this.getCategories = function () {
		return new Promise(function (resolve, reject) {
			request('http://rutube.ru/api/tags/?format=json', function (error, response, body) {
				resolve({provider: provider, categories: JSON.parse(body)});
			});
		});
	};
}
;
