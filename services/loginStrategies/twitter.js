module.exports = {
	init: function (router) {
		var config           = require('../../app/config'),
			passport         = require('passport'),
			TwitterStrategy = require('passport-twitter').Strategy;

		passport.use(new TwitterStrategy({
				consumerKey   : config.twitter.appId, // VK.com docs call it 'API ID'
				consumerSecret: config.twitter.secret,
				callbackURL   : "http://localhost:1337/auth/twitter/callback"
			},
			function (accessToken, refreshToken, profile, done) {
				return done(null, profile)
			}
		));

		router.get('/auth/twitter',
			passport.authenticate('twitter'));
		router.get('/auth/twitter/callback',
			passport.authenticate('twitter', {failureRedirect: '/'}),
			function (req, res) {
				res.redirect('/');
			});
	}
};
