module.exports = {
	init: function (router) {
		var config           = require('../../app/config'),
			passport         = require('passport'),
			FacebookStrategy = require('passport-facebook').Strategy;

		passport.use(new FacebookStrategy({
				clientID    : config.facebook.appId,
				clientSecret: config.facebook.secret,
				callbackURL : 'http://localhost:1337/auth/facebook/callback',
				enableProof : config.facebook.enableProof
			},
			function (accessToken, refreshToken, profile, done) {
				return done(null, profile)
			}
		));

		router.get('/auth/facebook',
				passport.authenticate('facebook'));
		router.get('/auth/facebook/callback',
				passport.authenticate('facebook', { failureRedirect: '/' }),
				function(req, res) {
					res.redirect('/');
				});
	}
};
