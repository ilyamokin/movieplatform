module.exports = {
	init: function (router) {
		var config           = require('../../app/config'),
			passport         = require('passport'),
			LinkedInStrategy = require('passport-linkedin').Strategy;

		passport.use(new LinkedInStrategy({
				consumerKey   : config.linkedin.appId, // VK.com docs call it 'API ID'
				consumerSecret: config.linkedin.secret,
				callbackURL   : "http://localhost:1337/auth/linkedin/callback"
			},
			function (accessToken, refreshToken, profile, done) {
				return done(null, profile)
			}
		));

		router.get('/auth/linkedin',
			passport.authenticate('linkedin'));
		router.get('/auth/linkedin/callback',
			passport.authenticate('linkedin', {failureRedirect: '/'}),
			function (req, res) {
				res.redirect('/');
			});
	}
};
