module.exports = {
	init: function (router, db) {
		var config        = require('../../app/config'),
			passport      = require('passport'),
			LocalStrategy = require('passport-local').Strategy,
			bcrypt        = require('bcrypt');

		passport.use(new LocalStrategy(
			function (username, password, done) {
				db.get('users').findOne({username: username, provider: 'local'}, ['username', 'password'], function (err, user) {
					if (err) { return done(err); }
					if (!user) {
						return done(null, false, {message: 'Incorrect username.'});
					}
					var hash = bcrypt.hashSync(password, config.salt);
					if (hash !== user.password) {
						return done(null, false, {message: 'Incorrect password.'});
					}
					return done(null, user);
				});
			}
		));
	}
};
