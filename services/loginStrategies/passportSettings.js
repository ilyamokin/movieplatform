module.exports = {
	setup: function (passport, app) {
		app
			.use(passport.initialize())
			.use(passport.session());

		passport.serializeUser(function (user, done) {
			done(null, user);
		});

		passport.deserializeUser(function (user, done) {
			done(null, user);
		});
		return app;
	}
};
