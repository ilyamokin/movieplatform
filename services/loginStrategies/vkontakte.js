module.exports = {
	init: function (router) {
		var config           = require('../../app/config'),
			passport         = require('passport'),
			VKontakteStrategy = require('passport-vkontakte').Strategy;

		passport.use(new VKontakteStrategy({
					clientID:     config.vkontakte.appId, // VK.com docs call it 'API ID'
					clientSecret: config.vkontakte.secret,
					callbackURL:  "http://localhost:1337/auth/vkontakte/callback"
				},
			function (accessToken, refreshToken, profile, done) {
				return done(null, profile)
			}
		));

		router.get('/auth/vkontakte',
				passport.authenticate('vkontakte'));
		router.get('/auth/vkontakte/callback',
				passport.authenticate('vkontakte', { failureRedirect: '/' }),
				function(req, res) {
					res.redirect('/');
				});
	}
};
