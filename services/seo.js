'use strict';
module.exports = {
	getRandomKeywords : function (words, seed) {
		var gen  = require('random-seed'),
			rand = gen.create(seed);
		return words.filter(function (word) {
			var val = rand(10);
			return val > 5;
		});
	},
	getRandomDigitPair: function (sumNumber, seed) {
		var gen      = require('random-seed'),
			rand     = gen.create(seed),
			firstVal = rand(sumNumber);
		return {
			firstNum : firstVal,
			secondNum: sumNumber - firstVal
		}
	},
	shuffle           : function (array, seed, take) {
		var _          = require('underscore'),
			gen        = require('random-seed'),
			rand       = gen.create(seed),
			randomIndex,
			arrayClone = _.clone(array),
			newArray   = [];
		take = take || array.length;

		while (arrayClone.length != 0 && newArray.length < take) {
			randomIndex = Math.floor(rand(arrayClone.length));
			newArray.push(arrayClone[randomIndex]);
			arrayClone.splice(randomIndex, 1);
		}

		return newArray;
	},
	getFilteredWords  : function (description) {
		var _          = require('underscore'),
			words      = description.match(/[()>\s]*(\w{4,})[()\s,]?/gmi),
			ignoreList = [
				'http'
			];

		if (!words || !words.length) {
			return [];
		}
		words = words.map(function (str) {
			return str.replace(/[.()\s, _-]/gmi, '');
		}).filter(function (str) {
			return ignoreList.indexOf(str) === -1;
		});
		return _.uniq(words);
	},
	getSeoKey         : function (text) {
		text = text
			.replace(/[^a-z ]*/gmi, '')
			.replace(/ *$/gmi, '')
			.replace(/ +/g, '-')
			.toLowerCase();

		return encodeURI(text);
	}
};