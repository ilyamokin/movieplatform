'use strict';

module.exports.YouTube = function (db) {
	var Youtube  = require("youtube-api"),
		config   = require("./../../app/config"),
		co       = require('co'),
		token    = config.youtube.token,
		ObjectID = require('mongodb').ObjectID,
		provider = this;

	this.searchMovies = function (ops) {
		return new Promise(function (resolve) {
			ops.key = token;
			ops.part = "snippet";
			ops.type = 'video';
			ops.videoSyndicated = true;
			ops.videoEmbeddable = true;
			ops.maxResults = ops.maxResults || 12;
			ops.pageToken = ops.pageToken || "";
			ops.order = "rating";

			Youtube.search.list(ops, function (err, result) {
				resolve({provider: provider, movies: result});
			});
		});
	};

	this.getVideoImage = co.wrap(function* (id) {
		return yield provider.getVideo(id, ['json.items.snippet.thumbnails.medium.url', 'json.localImagePath'])
	});

	this.updateImagePath = function (id, imagePath) {
		var movies = db.get('movies');
		return new Promise(function (resolve, reject) {
			movies.updateOne(
				{provider: 'youtube', providerId: id},
				{$set: {'json.localImagePath': imagePath}}, function (err, resp) {
					resolve({provider: provider});
				});
		});
	};

	this.getVideo = co.wrap(function* (id, fields) {
		var movies = db.get('movies'),
			video  = yield db.get('movies').findOne({provider: 'youtube', providerId: id}, fields || {});

		if (video) {
			return Promise.resolve(({provider: provider, rate: (video.rate / video.votes).toFixed(0), video: video.json, _id: video._id, comments: video.comments}));
		} else {
			return new Promise(function (resolve) {
				Youtube.videos.list({
					key : token,
					part: "snippet",
					id  : id
				}, function (err, result) {
					let _id  = new ObjectID(),
						rate = +((Math.random() * 10) % 3 + 2).toFixed(0);
					movies.insert({
						rate      : rate * 50,
						votes     : 50,
						provider  : 'youtube',
						providerId: id,
						json      : result,
						comments  : []
					});
					resolve({provider: provider, rate: 1, video: result, _id: _id, comments: []});
				});
			})
		}
	});

	this.getCategories = function (id) {
		return new Promise(function (resolve) {
			var ops = {part: "snippet", key: token};

			if (id) {ops.id = id;}
			else {ops.regionCode = 'US';}

			Youtube.videoCategories.list(ops, function (err, result) {
				resolve({provider: provider, categories: result});
			});
		});
	}
};